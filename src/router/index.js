import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Agent from "../views/Agent.vue";
import Listing from "../views/Listing.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/agent/:id",
    name: "Agent",
    component: Agent,
  },
  {
    path: "/listing/:id",
    name: "Listing",
    component: Listing,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
